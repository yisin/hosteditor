# Hosts编辑器（JS版）

#### 介绍
使用简易引擎JS版开发，纯JS语法开发的Hosts文件编辑器

#### 软件架构
1. 简易引擎JS版
2. html + css + js
3. windows系统底层API

#### 软件截图
![Hosts文件编辑器][1]


#### 安装教程

1. 下载安装[简易引擎JS版](https://pan.baidu.com/s/1hK_VfferHRpn4OmBI38WKA#提取码=tfdj)
2. 使用“简易引擎JS版-脚本打包器”将程序打包成xx.eejs文件
3. 双击*.eejs文件即可运行

#### 开发环境

1. 下载安装[简易引擎JS版](https://pan.baidu.com/s/1hK_VfferHRpn4OmBI38WKA#提取码=tfdj)
2. 双击运行“简易引擎JS版-开发调试器”程序
3. 将主入口页面（.html）拖放到“简易引擎JS版-开发调试器”程序窗口上即可调试运行

#### 参与贡献

1. 简易模块

#### 引擎相关

1. 简易引擎JS版官网 [eejs.yinsin.net](http://eejs.yinsin.net)
2. 引擎开发API [http://eejs.yinsin.net/api](http://eejs.yinsin.net/api)

#### 相关推荐

1. JS操作一键登录京东 [传送门](https://gitee.com/yisin/jdlogin)
2. JS开发Plants-VS-Zombies辅助 [传送门](https://gitee.com/yisin/zwdzjs1tool)

  [1]: http://eejs.yinsin.net/images/hostsEditor.jpg